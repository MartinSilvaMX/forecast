import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';

import json from './data/weather.json';
import { Line } from 'react-chartjs-2';
/*
var data;

var oReq = new XMLHttpRequest();
oReq.onload = reqListener;
oReq.open("get", "./data/weather.json", true);
oReq.send();

function reqListener(e) {
    data =       this.responseText;
    //JSON.parse(        );
    console.log("TEST:"+data);
}
*/

class Home extends Component {
  constructor() {
    super(...arguments);
    this.state = {
      content: this.viewA
    };
  }
  viewA =
    <div className="content">
      <h3>{json.query.results.channel.item.condition.temp}&deg;</h3>
      <h4>{json.query.results.channel.item.condition.text}</h4>
    </div>;
  viewB =
    <div className="content">
      <p>
        <b>Wind Speed:&nbsp;</b>{json.query.results.channel.wind.speed}
        <br />
        <b>Humidity:&nbsp;</b>{json.query.results.channel.atmosphere.humidity}
        <br />
        <b>Pressure:&nbsp;</b>{json.query.results.channel.atmosphere.pressure}
        <br />
        <b>Visibity:&nbsp;</b>{json.query.results.channel.atmosphere.visibility}
      </p>
    </div>;
  viewC = <div className="content" dangerouslySetInnerHTML={{ __html: json.query.results.channel.item.description.slice(0, -3) }}></div>;
  render() {
    return (
      <div className="Home">
        <h1>{json.query.results.channel.item.title.split(/ at /)[0]}</h1>
        <h2>{json.query.results.channel.item.condition.date}</h2>
        <div className="actions">
          <button onClick={ev => this.setState({ content: this.viewA })}>Main</button>
          <button onClick={ev => this.setState({ content: this.viewB })}>More</button>
          <button onClick={ev => this.setState({ content: this.viewC })}>Sumary</button>
        </div>
        {this.state.content}
      </div>
    );
  }
}

function App() {

  const data = {
    labels: [],
    text: [],
    datasets: [
      {
        label: 'High',
        data: [],
        borderColor: ['rgba(255,206,86,0.2)'],
        backgroundColor: ['rgba(255,206,86,0.2)'],
        pointBackgroundColor: ['rgba(255,206,86,0.2)'],
        pointBorderColor: ['rgba(255,206,86,0.2)']
      },
      {
        label: 'Low',
        data: [],
        borderColor: ['rgba(54,162,235,0.2)'],
        backgroundColor: ['rgba(54,162,235,0.2)'],
        pointBackgroundColor: ['rgba(54,162,235,0.2)'],
        pointBorderColor: ['rgba(54,162,235,0.2)']
      }
    ]
  };

  const options = {
    title: {
      display: true,
      text: 'Forecast',
      fontFamily: "'Ubuntu', sans-serif"
    },
    scales: {
      yAxes: [
        {
          ticks: {
            min: 30,
            max: 60,
            stepSize: 1,
            fontFamily: "'Ubuntu', sans-serif"
          }
        }
      ],
      xAxes: [
        {
          ticks: {
            fontFamily: "'Ubuntu', sans-serif"
          }
        }
      ]
    },
    legend: {
      labels: {
        // This more specific font property overrides the global property
        fontColor: 'black',
        fontFamily: "'Ubuntu', sans-serif"
      }
    },
    tooltips: {
      // Disable the on-canvas tooltip
      enabled: false,

      custom: function (tooltipModel) {
        // Tooltip Element
        var tooltipEl = document.getElementById('chartjs-tooltip');

        // Create element on first render
        if (!tooltipEl) {
          tooltipEl = document.createElement('div');
          tooltipEl.id = 'chartjs-tooltip';
          tooltipEl.innerHTML = '<table></table>';
          document.body.appendChild(tooltipEl);
        }

        // Hide if no tooltip
        if (tooltipModel.opacity === 0) {
          tooltipEl.style.opacity = 0;
          return;
        }

        // Set caret Position
        tooltipEl.classList.remove('above', 'below', 'no-transform');
        if (tooltipModel.yAlign) {
          tooltipEl.classList.add(tooltipModel.yAlign);
        } else {
          tooltipEl.classList.add('no-transform');
        }

        function getBody(bodyItem) {
          return bodyItem.lines;
        }

        // Set Text
        if (tooltipModel.body) {
          var titleLines = tooltipModel.title || [];
          var bodyLines = tooltipModel.body.map(getBody);

          var innerHtml = '<thead>';

          titleLines.forEach(function (title) {
            innerHtml += '<tr><th>' + title + '</th></tr>';
          });
          innerHtml += '</thead><tbody>';

          bodyLines.forEach(function (body, i) {
            console.log(tooltipModel, true, 2);
            console.log(data.text[tooltipModel.dataPoints[0].index]);
            //var colors = tooltipModel.labelColors[i];
            var style = 'background:' + tooltipModel.backgroundColor;
            style += '; border-color:' + tooltipModel.borderColor;
            style += '; border-width: 2px';
            var span = '<span style="' + style + '"></span>';
            innerHtml += '<tr><td>' + span + body + '&deg;&nbsp;' + data.text[tooltipModel.dataPoints[0].index] + '</td></tr>';
          });
          innerHtml += '</tbody>';

          var tableRoot = tooltipEl.querySelector('table');
          tableRoot.innerHTML = innerHtml;
        }

        // `this` will be the overall tooltip
        var position = this._chart.canvas.getBoundingClientRect();

        // Display, position, and set styles for font
        tooltipEl.style.opacity = 1;
        tooltipEl.style.position = 'absolute';
        tooltipEl.style.backgroundColor = tooltipModel.backgroundColor;
        tooltipEl.style.color = 'white';
        tooltipEl.style.border = '1px solid ' + tooltipModel.backgroundColor;
        tooltipEl.style.borderRadius = '3px';
        tooltipEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX + 'px';
        tooltipEl.style.top = position.top + window.pageYOffset + tooltipModel.caretY + 'px';
        tooltipEl.style.fontFamily = tooltipModel._bodyFontFamily;
        tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
        tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
        tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
        tooltipEl.style.pointerEvents = 'none';
      }
    },
    responsive: true
  };

  json.query.results.channel.item.forecast.forEach(item => {
    data.labels.push(item.day);
    data.datasets[0].data.push(item.high);
    data.datasets[1].data.push(item.low);
    data.text.push(item.text);
  })


  console.log(data, true, 2);
  return (
    <div className="App" >
      <div className="box w3-card-2">
        <Home />
        <hr style={{ width: '90%', margin: '50px auto' }} />
        <div
          style={{
            maxWidth: '100%',
            width: '400px',
            height: '300px',
            margin: 'auto'
          }}
        >
          <Line data={data} options={options} />
        </div>
      </div >
    </div >
  );
}

export default App;
